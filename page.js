$(document).ready(function () {

    console.log("lulz");
    $(".row-page").hide();
    $("#home-page").show();


    $(".menu-button").click(function () {
        $(".row-page").hide();
        console.log("id: "+this.id);
        switch (this.id){
            case "experience":
                $("#experience-page").show();
                break;
            case "about":
                $("#about-page").show();
                break;
            case "home":
                $("#home-page").show();
                break;
            case "contact":
                $("#contact-page").show();
                break;
            default:
                console.log("something weird happened. :\\");
        }

    });


    function hexEncode(input) {
        var hex, i;

        var result = "";
        for (i = 0; i < input.length; i++) {
            hex = input.charCodeAt(i).toString(16);
            result += ("000" + hex).slice(-4);
        }

        return result;
    }

    function hexDecode(input) {
        var j;
        var hexes = input.match(/.{1,4}/g) || [];
        var back = "";
        for (j = 0; j < hexes.length; j++) {
            back += String.fromCharCode(parseInt(hexes[j], 16));
        }

        return back;
    }

});